import * as pulumi from "@pulumi/pulumi";
import * as aws from "@pulumi/aws";
import * as cfg from "./config";

const fs = require("fs");
const path = require("path");
const mime = require("mime");

// Obtención de parametros de configuración
const siteBucketName = cfg.siteBucketName;
const siteDir = cfg.siteDir;
const targetDomain = cfg.targetDomain;

/////////////////////////////////////////////////////////////////////////////////
// Creación de la infraestructura S3 + ACM + CDN
/////////////////////////////////////////////////////////////////////////////////

//-------------------------------------------------------------------------------
// Creación de S3 Buckets
//-------------------------------------------------------------------------------

// Creación de S3 bucket del contenido web (AWS resource)
const siteBucket = new aws.s3.Bucket(siteBucketName, {
    acl: "private",
});

const siteBucketPublicAccessBlock = new aws.s3.BucketPublicAccessBlock("siteBucketPublicAccessBlock", {
    bucket: siteBucket.id,
    blockPublicAcls: true,
    blockPublicPolicy: true,
    ignorePublicAcls: true,
    restrictPublicBuckets: true,
});

// Creación de S3 bucket de logs de contenido web (AWS resource)
const logsBucket = new aws.s3.Bucket(`${siteBucketName}-logs`, {
    acl: "private",
});

const logsBucketPublicAccessBlock = new aws.s3.BucketPublicAccessBlock("logsBucketPublicAccessBlock", {
    bucket: logsBucket.id,
    blockPublicAcls: true,
    blockPublicPolicy: true,
    ignorePublicAcls: true,
    restrictPublicBuckets: true,
});

//-------------------------------------------------------------------------------
// Creación de ACM Certificates
//-------------------------------------------------------------------------------

// Creación de un AWS Provider en la región us-east-1
const useast1 = new aws.Provider("useast1", { region: "us-east-1" });

// Creación del certificado ACM en la region us-east-1
const certificateConfig: aws.acm.CertificateArgs = {
    domainName: targetDomain,
    validationMethod: "DNS",
    subjectAlternativeNames: [`www.${targetDomain}`],
};
const certificate = new aws.acm.Certificate("certificate", certificateConfig, { provider: useast1 });

// Obtención de Route 53 Hosted Zone del dominio "targetDomain"
const hostedZoneId = aws.route53.getZone({ name: `${targetDomain}.`, privateZone: false }, {async: true}).then(zone => zone.zoneId);

// Creación de un registro DNS para probar la propiedad del dominio "targetDomain" al cual estamos requiriendo el certificado
// Para mayor información https://docs.aws.amazon.com/acm/latest/userguide/gs-acm-validate-dns.html
const certificateValidationDomain1 = new aws.route53.Record(`${targetDomain}-validation1`, {
    name: certificate.domainValidationOptions[0].resourceRecordName,
    zoneId: hostedZoneId,
    type: certificate.domainValidationOptions[0].resourceRecordType,
    records: [certificate.domainValidationOptions[0].resourceRecordValue],
    ttl: 300,
});

const certificateValidationDomain2 = new aws.route53.Record(`${targetDomain}-validation2`, {
    name: certificate.domainValidationOptions[1].resourceRecordName,
    zoneId: hostedZoneId,
    type: certificate.domainValidationOptions[1].resourceRecordType,
    records: [certificate.domainValidationOptions[1].resourceRecordValue],
    ttl: 300,
});

// Valiación del certificados del ACM vía registros DNS de Route 53
const certificateValidation = new aws.acm.CertificateValidation("certificateValidation", {
    certificateArn: certificate.arn,
    validationRecordFqdns: [certificateValidationDomain1.fqdn, certificateValidationDomain2.fqdn],
}, { provider: useast1 });

const certificateArn = certificateValidation.certificateArn;

//-------------------------------------------------------------------------------
// Creación de Cloudfront
//-------------------------------------------------------------------------------

const originAccessControl = new aws.cloudfront.OriginAccessControl("originAccessControl", {
    description: "This is needed to setup s3 policies and make s3 not public",
    originAccessControlOriginType: "s3",
    signingBehavior: "always",
    signingProtocol: "sigv4",
});

// Configuración "distributionArgs" para el CloudFront distribution. Documentación relevante:
// https://docs.aws.amazon.com/AmazonCloudFront/latest/DeveloperGuide/distribution-web-values-specify.html
// https://www.terraform.io/docs/providers/aws/r/cloudfront_distribution.html
const distributionArgs: aws.cloudfront.DistributionArgs = {
    enabled: true,
    isIpv6Enabled: false,

    // Especificamos el origin de la distribución, el S3 bucket.
    origins: [
        {
            originId: siteBucket.arn,
            domainName: siteBucket.bucketRegionalDomainName,
            originAccessControlId: originAccessControl.id,
        },
    ],

    defaultRootObject: "index.html",
    aliases: [targetDomain, `www.${targetDomain}`],

    // Cache Bahavior del cloudfront distribution basado en el request path
    defaultCacheBehavior: {
        targetOriginId: siteBucket.arn,

        viewerProtocolPolicy: "redirect-to-https",
        allowedMethods: ["GET", "HEAD", "OPTIONS"],
        cachedMethods: ["GET", "HEAD", "OPTIONS"],

        forwardedValues: {
            cookies: { forward: "none" },
            queryString: false,
        },

        minTtl: 0,
        defaultTtl: 3600,   // 10 minutos
        maxTtl: 86400,      // 24 horas
        compress: true,
    },

    // "100" implica menos presencia en algunas regiones, sin embargo es la más económica
    priceClass: "PriceClass_100",

    // Podemos personalizar error responses con la distribución de Cloudfront
    customErrorResponses: [
        { errorCode: 404, responseCode: 404, responsePagePath: "/404.html" },
    ],

    restrictions: {
        geoRestriction: { restrictionType: "none" },
    },

    viewerCertificate: {
        acmCertificateArn: certificateArn,      // Per AWS, ACM certificate must be in the us-east-1 region
        sslSupportMethod: "sni-only",
        minimumProtocolVersion: "TLSv1.2_2021",
    },

    loggingConfig: {
        bucket: logsBucket.bucketDomainName,
        includeCookies: false,
        prefix: "website/",
    },
};

const siteCDN = new aws.cloudfront.Distribution("siteCDN", distributionArgs);

// Politica para que el Cloudfront pueda acceder al bucket "siteBucket"
const siteBucketPolicy = new aws.s3.BucketPolicy("siteBucketPolicy", {
    bucket: siteBucket.id,
    policy: pulumi.all([siteCDN.arn, siteBucket.arn]).apply(([cloudfrontArn, bucketArn]) => JSON.stringify({
        Version: "2012-10-17",
        Statement: [{
            Sid: "AllowCloudFrontServicePrincipal",
            Effect: "Allow",
            Principal: {
                Service: "cloudfront.amazonaws.com",
            }, 
            Action: ["s3:GetObject"],       // Only allow Cloudfront read access
            Resource: [`${bucketArn}/*`],   // Give Cloudfront access to the entire bucket
            Condition: {
                StringEquals: {
                    "AWS:SourceArn": `${cloudfrontArn}`,
                }
            }
        }],
    })),
});

// /////////////////////////////////////////////////////////////////////////////////
// // Fin de creación de la infraestructura S3 + CDN
// /////////////////////////////////////////////////////////////////////////////////

// Obtenemos todos los archivos del directorio "siteDir"
const files = deepReadDir(siteDir);

// Creamos un S3 Object por cada uno de los archivos
for (const item of files) {
    let objectName = item.replace(siteDir, '');
    objectName = objectName.replace(/^\//, '');
    const siteObject = new aws.s3.BucketObject(objectName, {
        bucket: siteBucket,                             // reference the s3.Bucket object
        key: objectName,                                // object name once it is in the bucket
        source: new pulumi.asset.FileAsset(item),       // use FileAsset to point to a file
        contentType: mime.getType(item) || undefined,   // set the MIME type of the file
    });
}

// Función que obtiene todos los archivos recursivamente del directorio "dir"
function deepReadDir(dir: string) {
    // Definimos results como un array de strings
    let results = [] as string[];

    for (let item of fs.readdirSync(dir)) {
        let file = path.join(dir, item);
        let stat = fs.statSync(file);

        // Validamos si el item es un directorio o un archivo
        if (stat && stat.isDirectory()) {
            // Item es un directorio, recursivamente obtenemos los items
            results = results.concat(deepReadDir(file));
        } else {
            // Item es un archivo, agregamos a results
            results.push(file);
        }
    }
    return results;
}

// Exportamos propiedades del stack
export const siteS3BucketName = siteBucket.id;
export const logsS3Bucketname = logsBucket.id;
export const siteCDNDomainName = siteCDN.domainName;
