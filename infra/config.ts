import * as pulumi from "@pulumi/pulumi";

// Información de configuración
const config = new pulumi.Config();
export const siteDir = config.require("siteDir");

export const siteBucketName = "website-innovate";
export const targetDomain = "innovate.pe";