#!/bin/bash

# configuramos variables
WEBSITE=static-website

# inicializar un nuevo site llamado "static_website"
cd /app
npm init gatsby -y -- -y -ts $WEBSITE

# substituir "gatsby develop" por "gatsby develop -H 0.0.0.0" para poder exponer el servicio desde el contenedor
sed -i -e 's/gatsby develop/gatsby develop -H 0.0.0.0/g' $WEBSITE/package.json
