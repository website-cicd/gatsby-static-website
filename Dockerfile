# Construcción de imagen node-alpine
FROM node:18-alpine3.16 as node
FROM alpine:3.16 as alpine

# =================== #
#  Install Node 18  #
# =================== #
COPY --from=node /usr/lib /usr/lib
COPY --from=node /usr/local/share /usr/local/share
COPY --from=node /usr/local/lib /usr/local/lib
COPY --from=node /usr/local/include /usr/local/include
COPY --from=node /usr/local/bin /usr/local/bin

RUN npm config set legacy-peer-deps true
RUN apk add --update python3 git build-base sed curl

# Construcción de imagen pulumi-alpine
FROM alpine as pulumi

# ====================== #
#  Install pulumi v3.48  #
# ====================== #
ENV PATH "$PATH:/root/.pulumi/bin"
RUN curl -fsSL https://get.pulumi.com | sh

# =================== #
#  Install AWSCli v2  #
# =================== #
COPY --from=devopscorner/aws-cli:latest /usr/local/aws-cli/ /usr/local/aws-cli/
COPY --from=devopscorner/aws-cli:latest /usr/local/bin/ /usr/local/bin/